<?php
/**
 * Created by PhpStorm.
 * User: saber
 * Date: 2020/3/9
 * Time: 9:59
 */


require_once "common.php";

use GuzzleHttp\Client;
use Swoole\Process;
use Swoole\Table;

$global_table = new Table(2 * 10);
$global_table->column("title", Table::TYPE_STRING, 100);
$global_table->column("sum", Table::TYPE_INT);
$key = $global_table->create();
echo $key . "\n";

$network_request_process = new Process(function () use ($global_table) {
    $client = new Client();
    $responce = $client->get("http://www.baidu.com");
    if ($responce->getStatusCode() != 200) {
        echo "request error";
        exit(1);
    } else {
        $matched = preg_match('/<title>([^<]+)<\/title>/', (string)$responce->getBody(), $result);
        if (!$matched) {
            echo "not matched";
            exit(2);
        } else {
            set_global("title", $result[1]);
            print_global();
            $global_table->set(1, ["title" => $result[1]]);
            exit(0);
        }
    }
});


$calculate_process = new Process(function () use ($global_table) {
    $sums = 0;
    for ($i = 0; $i < 100000; $i++) {
        $sums += $i;
    }
    set_global("sum", 12);
    print_global();
    $global_table->set(1, ["sum" => $sums]);
    exit(0);
});

$network_request_process->start();
$calculate_process->start();

$process_to_wait = 2;

while ($process_to_wait) {
    Process::wait(true);
    $process_to_wait--;
}


print_r($global_table->get(1));





